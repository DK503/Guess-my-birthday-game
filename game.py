from random import randint

name = input("hi! whats your name?")

for guess_num in range(5):
    month_number = randint(1, 12)
    year_number = randint(1924, 2004)
    print(f"Guess {guess_num} :", name, "were you born in",
       month_number, "/", year_number, "?")

    response = input(" yes or no ")
    if response == "yes":
        print("I got it!")
        exit()
    elif guess_num == 5:
        print("I have other things to do. Good bye.")
    else:
        print("No! lets try again!")
